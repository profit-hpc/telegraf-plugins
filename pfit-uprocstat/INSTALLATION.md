# PFIT-uprocstat Input Plugin Installation and Activation

1. Add the pfit-uprocstat directory and its content to the plugins/inputs
   subdirectory in the telegraf source code.

2. Import the pfit-uprocstat directory in plugins/inputs/all/all.go.

3. Rebuild telegraf by running "make" in telegraf's main source directory.

   (For building a new RPM package, use the build.py script in the scripts 
    subdirectory, e.g.:

    ./scripts/build.py --package --platform=linux --arch=amd64 --version=2017-11-23
   
   The new RPM packages can then be found in the telegraf/build 
   subdirectory.)

4. After installing the new telegraf binary create a new telegraf.conf 
   file, e.g.:

   ./telegraf config > ../etc/telegraf.conf.new

   and activate the pfit-uprocstat plugin in the "INPUT PLUGINS" section
   by removing the comment before "[[processors.pfit-uprocstat]]".

   Configure the lowest userid whose processes should be monitored and, 
   if necessary, also the users whose processes should be ignored.

   (Do not forget the activate the other plugins, you are using, as well.)


### Author:

Guido Laubender, laubender@zib.de, +49 30 84185-214

### Version:

2018-08-07

