package pfituprocstat

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"github.com/shirou/gopsutil/process"
)

//NativeFinder uses gopsutil to find processes
type NativeFinder struct {
}

//NewNativeFinder ...
func NewNativeFinder() (PIDFinder, error) {
	return &NativeFinder{}, nil
}

//Uid will return all pids for the given user
func (pg *NativeFinder) Uid(user string) ([]PID, error) {
	var dst []PID
	procs, err := process.Processes()
	if err != nil {
		return dst, err
	}
	for _, p := range procs {
		username, err := p.Username()
		if err != nil {
			//skip, this can happen if we don't have permissions or
			//the pid no longer exists
			continue
		}
		if username == user {
			dst = append(dst, PID(p.Pid))
		}
	}
	return dst, nil
}

//PidFile returns the pid from the pid file given.
func (pg *NativeFinder) PidFile(path string) ([]PID, error) {
	var pids []PID
	pidString, err := ioutil.ReadFile(path)
	if err != nil {
		return pids, fmt.Errorf("Failed to read pidfile '%s'. Error: '%s'",
			path, err)
	}
	pid, err := strconv.Atoi(strings.TrimSpace(string(pidString)))
	if err != nil {
		return pids, err
	}
	pids = append(pids, PID(pid))
	return pids, nil

}

func (pg *NativeFinder) FindAll(ignore_users []string, start_from int) ([]PID, error) {
	var pids []PID

	procs, err := process.Processes()
	if err != nil {
		return pids, err
	}

	for _, p := range procs {
		uid := -1
		uids, err := p.Uids()
		if err == nil {
			uid = int(uids[0])
		}

		//skip users with IDs less than intended
		if uid < start_from {
			continue
		}

		user, err := p.Username()
		if err != nil {
			//skip, no permissions or no pid
			continue
		}
		//if a user should be ignored, skip it
		if pg.isInArray(user, ignore_users) {
			continue
		}

		//otherwise monitor it
		pids = append(pids, PID(p.Pid))
	}

	return pids, nil
}

func (pg *NativeFinder) isInArray(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
