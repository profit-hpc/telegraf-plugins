# Testing the plugins with Docker container

Build the container from the root of this repository:
```
docker build -t pfit-telegraf-plugins-test -f test/docker/Dockerfile .
```

Run the container to get the output of the plugins:
```
docker run pfit-telegraf-plugins-test
```
