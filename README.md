# Telegraf plugins

This repository contains Telegraf plugins used in ProfIT project.

## Plugin types

[Input Plugins](https://docs.influxdata.com/telegraf/v1.8/plugins/inputs) used in the project can be divided into 2 groups:

*  `native` plugins
*  `external` plugins

This repository contains plugins which were elaborated or modified to fulfill requirements of the project. Most of them are *external* plugins.

#### Native plugins
Native plugins for Telegraf are written in [Go](https://golang.org) and compiled with Telegraf. Because plugins require to be compiled they are not easily upgradable compared to *external plugins*. But at the same time they might work faster for particular tasks.

#### External plugins
External plugins are plugins written as shell or interpreted programming language scripts or even as executable binaries which run by Telegraf's [exec](https://github.com/influxdata/telegraf/tree/master/plugins/inputs/exec) input plugin. They are the fastest way to deploy the plugin, since for activation only configuration file should be altered and Telegraf daemon restarted.  
