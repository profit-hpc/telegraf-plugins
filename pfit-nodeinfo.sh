#!/bin/bash
# This will report the node info needed for ProfiT project
# Mostly CPU and Memory information
#
# Created by: Khuziyakhmetov, Azat <azat.khuziyakhmetov@gwdg.de>
# date: September 25, 2018

isdebug=false # set it `true` to get debug info when calling from a terminal

HOSTNAME="$(hostname -s)"

debug() {
  if $isdebug; then
    echo "$1"
  fi
}

INTS="sockets cores_per_socket threads_per_core main_mem"
must_be_int() {
  for m_test in $INTS; do
    if [[ "$1" == "$m_test" ]]; then
        return 0
    fi
  done
  return 1
}

ICPU=`lscpu`
IMEM=`cat /proc/meminfo`

declare -A metrics

metrics["cpu_model"]=`grep "Model name" <<< "$ICPU" | awk -F: '{gsub(/^[ \t]+/,"",$2); print $2}' `

metrics["sockets"]=`grep "Socket(s)" <<< "$ICPU" | awk '{print $2}'`

metrics["cores_per_socket"]=`grep "Core(s) per socket" <<< "$ICPU" | awk '{print $4}'`

metrics["threads_per_core"]=`grep "Thread(s) per core" <<< "$ICPU" | awk '{print $4}'`

metrics["main_mem"]=`grep MemTotal <<< "$IMEM" | awk '{printf("%.0f", $2 * 1024)}'`

if $isdebug; then
  for m in ${!metrics[@]}; do
    debug "$m: ${metrics[$m]}"
  done
fi

out="pfit-nodeinfo,host=${HOSTNAME}"

isfirst=true
for m in ${!metrics[@]}; do
  if $isfirst; then
    out="$out "
    isfirst=false
  else
    out="$out,"
  fi

  mval="${metrics[$m]}"
  re='^[0-9]+$'
  if must_be_int "$m"; then
    if [[ $mval =~ $re ]] ; then
      out="$out$m=$mval"
    else
      exit 3
    fi
  else
    out="$out$m=\"$mval\""
  fi
done

echo $out
