#!/bin/bash
#This will report some custom BeeGFS info to telegraf/influx
#
# Created by: Khuziyakhmetov, Azat <azat.khuziyakhmetov@gwdg.de>
# date: October, 2019

isdebug=false # set it `true` to get debug info when calling from a terminal

HOSTNAME="$(hostname -s)"

# !!!NEED TO BE SET TO THE TEMPLATE IN YOUR BEEGFS SYSTEM!!!
# The names example can be found by executing the command:
# beegfs-ctl --clientstats --names --nodetype=storage
HOSTNAMETMP="([0-9a-zA-Z-]+)\.ib\.gwdg\.cluster"

BGFSLOGPATH=/dev/shm/beegfs-logger

debug() {
  if $isdebug; then
    echo "$1"
  fi
}

the_right_host() {
  if command -v beegfs-ctl >/dev/null 2>&1; then
    return 0
  else
    return 1
  fi
}

if ! the_right_host; then
  exit 0
fi

# Create directory if doesn't exist
if [ ! -d "$BGFSLOGPATH" ]; then
  mkdir $BGFSLOGPATH
fi

CURTS=$(date +%s)
beegfs_command="beegfs-ctl --clientstats --names --nodetype=storage --interval=0 --maxlines=1000"
data=$( $beegfs_command | egrep $HOSTNAMETMP)

while read -r line; do
  [[ "$line" =~ $HOSTNAMETMP ]]
  BGFSHOSTNAME=${BASH_REMATCH[1]}
  debug "processing line: $line"
  debug "host $BGFSHOSTNAME was found"

  if [ "$BGFSHOSTNAME" == "" ]; then continue; fi

  BGFSLOGFILE=$BGFSLOGPATH/${BGFSHOSTNAME}.beegfs.log

  # Create file if doesn't exist
  if [ ! -w "$BGFSLOGFILE" ]; then
    touch $BGFSLOGFILE
  fi

  # Create necessary arrays
  declare -A prev_metrics
  declare -A aggr_metrics
  declare -A metrics

  # Read previous metrics
  PREVTS=0
  debug "Previous measurements:"
  while IFS== read -r key value; do
      if [ "$key" = "" ]; then continue; fi
      if [ "$key" = "timestamp" ]; then PREVTS=$value; fi
      if [ "$value" = "" ]; then value=0; fi
      prev_metrics[$key]=$value
      debug "$key = $value"
  done < "$BGFSLOGFILE"

  # Parse the current measurements
  debug "Parsing current measurments"
  prev_is_val=false
  prev_val=0
  for val in $line; do
    if $( echo "$val" | grep -q -E "\[.*\]" ); then
      m=$(echo $val | sed 's/^\[\(.*\)\]$/\1/')
      aggr_metrics[$m]=$prev_val
      debug "$m = $prev_val"
    fi
    prev_val="$val"
  done

  # Calculating current measurement
  for m in ${!aggr_metrics[@]}; do
    cur_metric=${aggr_metrics[$m]}
    prev_metric=${prev_metrics[$m]}

    if [ "$prev_metric" = "" ]; then prev_metric=0; fi

    diff=`bc <<< "scale=0; ($cur_metric - $prev_metric)/1"`

    debug "$m: ( $cur_metric - $prev_metric ) = $diff"

    if [ "$diff" -ge "0" ]; then
      metrics[$m]=$diff
    else
      metrics[$m]=$cur_metric
    fi
  done

  out="beegfs_clients,host=${BGFSHOSTNAME}"

  isfirst=true
  for m in ${!aggr_metrics[@]}; do
    if $isfirst; then
      out="$out "
      isfirst=false
    else
      out="$out,"
    fi

    out="$out$m=${metrics[$m]}"
  done

  echo $out

  # save the measurements in the file
  mf=""
  for m in ${!aggr_metrics[@]}; do
    mf="$m=${aggr_metrics[$m]}\n$mf"
  done
  mf="timestamp=$CURTS\n$mf"
  echo -e "$mf" > $BGFSLOGFILE
  debug "measurements are exported"
done < <(printf '%s\n' "$data")

exit 0
