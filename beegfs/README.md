# BeeGFS Plugin

This plugin is a shell script which outputs InfluxDB compatible output for Telegraf about BeeGFS storage usage per host.
The script can be used as an input plugin of Telegraf.

## Usage

Set the hostname template in the script and configure it as a script plugin in Telegraf. An example of hostnames could be found by executing the command below.
```
beegfs-ctl --clientstats --names --nodetype=storage --interval=0
```

## Command

The script collects the data using the following command:
```
beegfs-ctl --clientstats --names --nodetype=storage --interval=0 --filter=HOSTNAME
```

## Output
The script outputs all nonzero measurements, which include:
- operations Read
- operations Write
- MiB Read
- MiB Write

## Behavior
The values are the *average* per second. If Telegraf configured to measure every 10 seconds, then the collected value is divided by 10.

The scripts also uses /dev/shm as temporary storage for the previous measurements, since the command above outputs only accumulated values.
