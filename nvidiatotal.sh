#!/bin/bash
#This will report some custom NVIDIA info to telegraf/influx
#
# Created by: Khuziyakhmetov, Azat <azat.khuziyakhmetov@gwdg.de>
# date: November 13, 2017

# CUDA location
CUDA_HOME=/cm/local/apps/cuda/libs/current
HOSTNAME="$(hostname -s)"

the_right_host_among() {
  ISIN=false
  for item in $1
  do
    if [ "$HOSTNAME" == "$item" ]; then
      ISIN=true
      break
    fi
  done

  if $ISIN; then
    return 0
  else
    return 1
  fi
}


#if ! the_right_host_among "$1"; then
#  exit 0
#fi

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CUDA_HOME/lib64
smi=$CUDA_HOME/bin/nvidia-smi

gpu_params="index,gpu_name,gpu_bus_id,fan.speed,pstate,memory.total,memory.used,utilization.gpu,utilization.memory,encoder.stats.sessionCount,encoder.stats.averageFps,encoder.stats.averageLatency,ecc.errors.corrected.volatile.total,ecc.errors.corrected.aggregate.total,ecc.errors.uncorrected.volatile.total,ecc.errors.uncorrected.aggregate.total,retired_pages.sbe,retired_pages.dbe,temperature.gpu,power.draw,power.limit,enforced.power.limit,clocks.gr,clocks.sm,clocks.mem,clocks.video"
gpu_stats=`$smi --format=csv,noheader,nounits --query-gpu=$gpu_params`

proc_params="gpu_name,gpu_bus_id,pid,name,used_memory"
proc_stats=`$smi --format=csv,noheader,nounits --query-compute-apps=$proc_params`

cpu_params="uname,pcpu,pmem,maj_flt,min_flt,nlwp,rss,vsz"

gpu_param_idx=( ${gpu_params//,/ } )
proc_param_idx=( ${proc_params//,/ } )
cpu_param_idx=( ${cpu_params//,/ } )

timestamp=$(($(date +%s) * 1000000000))

while read -r line; do
  cnt=0
  declare -A gpu_vals

  while read -r val; do
    gpu_vals[${gpu_param_idx[$cnt]}]=`[ "$val" == "[Not Supported]" -o "$val" == "[Unknown Error]" -o "$val" == "[N/A]" ] && echo -1 || echo $val`
    cnt=$((cnt+1))
  done <<< "$(echo $line | tr "," "\n")";

  bus=`echo ${gpu_vals[gpu_bus_id]} | cut -d':' -f2`
  gpu_name=${gpu_vals[gpu_name]// /\\ }
  pstate=${gpu_vals[pstate]//P/}
  echo "nvidia_gpu,host=${HOSTNAME},gpu_name=$gpu_name,bus=$bus fan.speed=${gpu_vals[fan.speed]},pstate=$pstate,memory.total=${gpu_vals[memory.total]},memory.used=${gpu_vals[memory.used]},utilization.gpu=${gpu_vals[utilization.gpu]},utilization.memory=${gpu_vals[utilization.memory]},encoder.stats.sessionCount=${gpu_vals[encoder.stats.sessionCount]},encoder.stats.averageFps=${gpu_vals[encoder.stats.averageFps]},encoder.stats.averageLatency=${gpu_vals[encoder.stats.averageLatency]},ecc.errors.corrected.volatile.total=${gpu_vals[ecc.errors.corrected.volatile.total]},ecc.errors.corrected.aggregate.total=${gpu_vals[ecc.errors.corrected.aggregate.total]},ecc.errors.uncorrected.volatile.total=${gpu_vals[ecc.errors.uncorrected.volatile.total]},ecc.errors.uncorrected.aggregate.total=${gpu_vals[ecc.errors.uncorrected.aggregate.total]},retired_pages.sbe=${gpu_vals[retired_pages.sbe]},retired_pages.dbe=${gpu_vals[retired_pages.dbe]},temperature.gpu=${gpu_vals[temperature.gpu]},power.draw=${gpu_vals[power.draw]},power.limit=${gpu_vals[power.limit]},enforced.power.limit=${gpu_vals[enforced.power.limit]},clocks.gr=${gpu_vals[clocks.gr]},clocks.sm=${gpu_vals[clocks.sm]},clocks.mem=${gpu_vals[clocks.mem]},clocks.video=${gpu_vals[clocks.video]} $timestamp"

done <<< "$gpu_stats"

while read -r line; do
  cnt=0
  declare -A proc_vals

  while read -r val; do
    proc_vals[${proc_param_idx[$cnt]}]=`[ "$val" == "[Not Supported]" -o "$val" == "[Unknown Error]" -o "$val" == "[N/A]" ] && echo -1 || echo $val`
    cnt=$((cnt+1))
  done <<< "$(echo $line | tr "," "\n")";

  if [ -z "${proc_vals[pid]}" ]; then continue; fi;

  if [ "${proc_vals[pid]}" -eq  "-1" ]; then continue; fi;

  JOBID=`strings /proc/${proc_vals[pid]}/environ | grep -E "(SLURM|LSF)_JOBID=" | cut -d= -f2`

  if [ "$JOBID" == "" ]; then JOBID=-1; fi;

  cpu_stats=`ps --no-headers -o $cpu_params -p ${proc_vals[pid]}`

  cnt=0
  declare -A cpu_vals

  for val in $cpu_stats; do
    cpu_vals[${cpu_param_idx[$cnt]}]="$val"
    cnt=$((cnt+1))
  done

  if [ -z "${cpu_vals[uname]}" ]; then continue; fi;

  gpu_name=${proc_vals[gpu_name]// /\\ }
  bus=`echo ${proc_vals[gpu_bus_id]} | cut -d':' -f2`

  echo "nvidia_proc,host=${HOSTNAME},gpu_name=${gpu_name},bus=${bus},username=${cpu_vals[uname]},JOBID=${JOBID} pid=${proc_vals[pid]},name=\"${proc_vals[name]}\",used_memory=${proc_vals[used_memory]},cpu_pcpu=${cpu_vals[pcpu]},cpu_pmem=${cpu_vals[pmem]},cpu_maj_flt=${cpu_vals[maj_flt]},cpu_min_flt=${cpu_vals[min_flt]},cpu_nlwp=${cpu_vals[nlwp]},cpu_rss=${cpu_vals[rss]},cpu_vsz=${cpu_vals[vsz]} $timestamp"

  timestamp=$((timestamp+1000000000))
done <<< "$proc_stats"
